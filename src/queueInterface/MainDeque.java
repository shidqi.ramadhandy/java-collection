package queueInterface;

import java.util.Deque;
import java.util.LinkedList;

public class MainDeque {
    public static void main(String[] args) {
        Deque<String> stack = new LinkedList<>();
        stack.offerLast("Mamad");
        stack.offerLast("Zul");
        stack.offerLast("Loed");
        stack.offerLast("Popol");

        for (var item = stack.pollLast(); item != null; item = stack.pollLast()){
            System.out.println(item);
        }


    }
}
