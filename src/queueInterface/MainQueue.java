package queueInterface;

import Model.Student;

import java.util.ArrayDeque;
import java.util.PriorityQueue;
import java.util.Queue;

public class MainQueue {
    public static void main(String[] args) {
        Queue<String> queue = new ArrayDeque<>(10);
        queue.offer("Joko");
        queue.offer("Dedi");
        queue.offer("Wiro");
        queue.offer("Sableng");

        for (String next = queue.poll(); next != null; next = queue.poll()) {
            System.out.println(next);
        }
        System.out.println(queue.size());


    Queue<String> queue2 = new PriorityQueue<>();
        queue2.offer("Ahmad");
        queue2.offer("Santoso");
        queue2.offer("Ardi");
        queue2.offer("Lutfianto");

        for (String next = queue2.poll(); next != null; next = queue2.poll()) {
        System.out.println(next);
    }
        System.out.println(queue2.size());




    }

    }

