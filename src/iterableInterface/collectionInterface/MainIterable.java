package iterableInterface.collectionInterface;

import Model.Employee;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class MainIterable {

    public static void main(String[] args) {

        Iterable<String> names = List.of("Satella", "Emilia", "Endoh", "Alto", "Adam Suseno");
        for (var name: names
             ) {
            System.out.println(name);
        }

        System.out.println("Iterable");
        Iterable<Employee> employees = Arrays.asList(
                new Employee(01,"Shidqi", "CEO", true ),
                new Employee(02, "Nabila", "Sekretaris", true),
                new Employee(03, "Bambang", "Keuangan",true),
                new Employee(04, "Ayana", "akuntan", false)
        );

        for (var employee: employees
             ) {
            System.out.println(employee);
        }

        System.out.println("Iterator");
        Iterator<Employee>employeeIterator= employees.iterator();
        while (employeeIterator.hasNext()){
            System.out.println(employeeIterator.next());
        }

    }
}
