package iterableInterface.collectionInterface;

import Model.Employee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class MainCollection {
    public static void main(String[] args) {

        Collection<String> names = new ArrayList<>();
        names.add("Shidqi");
        names.add("Juleha");
        names.add("Mamad");

        names.addAll(List.of("Zul","Rai","Dinda"));

        //loop
        System.out.println("Menambahkan nama di collection");
        for (var name:names){
            System.out.println(name);
        }
        //menegcek
        System.out.println("Mengece nama Mamad, Zul dan Rai  ");
        System.out.println(names.contains("Mamad"));
        System.out.println(names.containsAll(Arrays.asList("Zul", "Rai")));

        System.out.println("setelah dihapus :");
        names.remove("Juleha");
        names.removeAll(List.of("Mamad","Zul", "Rai"));
        for (var name:names){
            System.out.println(name);
        }

        //mengecek
        System.out.println("Mengecek kembali nama Mamad, Zul dam Rai ");
        System.out.println(names.contains("Mamad"));
        System.out.println(names.containsAll(Arrays.asList("Zul", "Rai")));



        //class
        Collection<Employee> employees = new ArrayList<>();
        employees.add(new Employee(01, "mamank", "Manager",true));
        employees.add(new Employee(02, "racing", "Pembalap",true));
        employees.add(new Employee(03, "Kesbor", "Jawa",true));

        employees.addAll(Arrays.asList(
                new Employee(04,"hitam", "kecap", false),
                new Employee(04,"ireng", "kamu", false)

                ));
        System.out.println("List nama karyawan : ");
        for (var e: employees
             ) {
            System.out.println(e);
        }
//        System.out.println("Mengecek semua karywan : ");
//        System.out.println(employees.containsAll(Arrays.asList(
//                new Employee(01, "mamank", "Manager",true),
//                new Employee(02, "racing", "Pembalap",true),
//                new Employee(03, "Kesbor", "Jawa",true),
//                new Employee(04,"hitam", "kecap", false),
//                new Employee(04,"ireng", "kamu", false)
//        )));

    }
}
