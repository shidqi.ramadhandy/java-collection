package iterableInterface.setCollection;

import Model.JobEnum;

import java.util.EnumSet;
import java.util.Set;

public class MainImuttableSet {
    public static void main(String[] args) {
        Set<JobEnum> job = EnumSet.allOf(JobEnum.class);
        System.out.println(job);


        Set<String> immutSet = Set.of("Hareruya", "win", "Aceng");
        immutSet.remove("win"); // error
        immutSet.add("Buffa"); // error

    }
}
