package iterableInterface.setCollection;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class MainSet {
    public static void main(String[] args) {
        Set<String> names = new HashSet<>();
        names.add("Talulah");
        names.add("Amiya");
        names.add("Bnuy");
        names.add("Frostnova");
        names.add("We");
        names.addAll(List.of("Low", "Man", "Dik"));

        System.out.println(names);
        System.out.println("Size : "+names.size());

        Set<String> cars = new LinkedHashSet<>();
        cars.add("Civic");
        cars.add("Baleno");
        cars.add("Innova");
        cars.addAll(List.of("Juke", "Kijang", "CRV"));

        System.out.println(cars);
        System.out.println("Size : "+cars.size());

    }

}
