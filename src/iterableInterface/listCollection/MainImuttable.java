package iterableInterface.listCollection;

import Model.Student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainImuttable {
    public static void main(String[] args) {
        Student student = new Student("Jepri",
                Arrays.asList("Makan", "Tidur", "Membantu Orangtua"));

        System.out.println(student);

        List<String> hobbies = new ArrayList<>(student.getHobi());
        hobbies.add("Pacaran");
        student.setHobi(hobbies);
        System.out.println(student);

    }
}
