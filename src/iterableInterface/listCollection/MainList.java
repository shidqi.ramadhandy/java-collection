package iterableInterface.listCollection;

import Model.Employee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainList {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>(Arrays.asList("Shidqi", "Zubaedah", "Siti Robiulawal", "Juned"));
        System.out.println(names);

        names.set(2, "Qiqi");
        names.set(0,"Michael");
        System.out.println(names);

        List<Employee> employees = new ArrayList<>(Arrays.asList(
           new Employee(01,"Kusanali", "Archon", true),
                new Employee(02,"Michael", "Arcangel", true ),
                new Employee(04, "Bael", "Demon", true)
        ));

        for (int i = 0; i < employees.size(); i++) {
            System.out.println("Index ke "+i+ " : " +employees.get(i));

        }
        System.out.println("\nSetalah Diubah");
        employees.set(1, new Employee(1, "Barbatos","Demon", false ));
        for(int i=0; i < employees.size(); i++) {
            System.out.println("Index ke "+i +" : "+employees.get(i));
        }


    }
}
