package iterableInterface.sortedSet;

import Model.Student;

import java.util.*;
import java.util.stream.Collectors;

public class MainNavigableSet {
    public static void main(String[] args) {
        NavigableSet<Student> students = new TreeSet<>(new StudentComparator());
        students.add(new Student(1,"dedi", "Cikoronjo"));
        students.add(new Student(2,"mijwar", "Cikarang"));
        students.add(new Student(3,"dedi", "Jakarta"));
        students.add(new Student(4,"cahyadi", "Bekasi"));

        for(Student s:students ){
            System.out.println(s);
    }

        System.out.println("\nNavigableSet dibalik");
        List<Student> studentNav = students.stream().sorted(Comparator.comparing(Student::getNama).reversed())
                .collect(Collectors.toList());
        for (Student s: studentNav){
            System.out.println(s);
        }

    }

}