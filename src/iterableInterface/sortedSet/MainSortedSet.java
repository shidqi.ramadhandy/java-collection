package iterableInterface.sortedSet;

import Model.Student;

import java.util.*;

public class MainSortedSet {
    public static void main(String[] args) {
        SortedSet<Student>students = new TreeSet<>(new StudentComparator());
        students.add(new Student(1,"dedi", "Cikoronjo"));
        students.add(new Student(2,"mijwar", "Cikarang"));
        students.add(new Student(3,"dedi", "Jakarta"));
        students.add(new Student(4,"cahyadi", "Bekasi"));

        for(Student s:students ){
            System.out.println(s);
        }
    }
}
