package Model;

public class Employee {

    private Integer employeeId;
    private String name;
    private String position;
    private Boolean karyawanTetap;

    public Employee() {
    }

    public Employee(Integer employeeId, String name, String position, Boolean karyawanTetap) {
        this.employeeId = employeeId;
        this.name = name;
        this.position = position;
        this.karyawanTetap = karyawanTetap;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Boolean getKaryawanTetap() {
        return karyawanTetap;
    }

    public void setKaryawanTetap(Boolean karyawanTetap) {
        this.karyawanTetap = karyawanTetap;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Employee{");
        sb.append("employeeId=").append(employeeId);
        sb.append(", name='").append(name).append('\'');
        sb.append(", position='").append(position).append('\'');
        sb.append(", karyawanTetap=").append(karyawanTetap);
        sb.append('}');
        return sb.toString();
    }
}
