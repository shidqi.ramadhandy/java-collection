package Model;

import java.util.List;
import java.util.StringJoiner;

public class Student {

    private int idStudent;

    private String nama;

    private String alamat;
    private List<String> hobi;


    public Student(String nama, List<String> hobi) {
        this.nama = nama;
        this.hobi = hobi;
    }

    public Student(int idStudent, String nama, String alamat) {
        this.idStudent = idStudent;
        this.nama = nama;
        this.alamat = alamat;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public List<String> getHobi() {
        return hobi;
    }

    public void setHobi(List<String> hobi) {
        this.hobi = hobi;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Student.class.getSimpleName() + "[", "]")
                .add("idStudent=" + idStudent)
                .add("nama='" + nama + "'")
                .add("alamat='" + alamat + "'")
                .add("hobi=" + hobi)
                .toString();
    }
}
