package mapInterface;

import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public class MainMap {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("firstname", "Leonal");
        map.put("midname", "Messi");
        map.put("lastname", "Muska");

        System.out.println(map.get("firstname"));
        System.out.println(map.get("midname"));
        System.out.println(map.get("lastname"));


        Map<Integer, Integer> map1 = new WeakHashMap<>();
        for (int i = 0; i < 100000; i++) {
        map1.put(i,i);
        }
        System.gc();
        System.out.println(map1.size());

    }

}
